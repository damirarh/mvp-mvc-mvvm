﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using MVC.Service;

namespace MVC.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {
            using (var proxy = new TodoServiceClient())
            {
                return View(await proxy.GetTodoItemsAsync());
            }
        }

        [HttpGet]
        public async Task<ActionResult> Edit(int id)
        {
            LoadPriorities();

            using (var proxy = new TodoServiceClient())
            {
                return View(await proxy.GetTodoItemAsync(id));
            }
        }

        [HttpPost]
        public async Task<ActionResult> Edit(TodoItem item)
        {
            using (var proxy = new TodoServiceClient())
            {
                await proxy.SaveTodoItemAsync(item);
                return View("Index", await proxy.GetTodoItemsAsync());
            }
        }

        public ActionResult New()
        {
            LoadPriorities();
            return View("Edit", new TodoItem());
        }

        private void LoadPriorities()
        {
            ViewBag.Priorities = Enum.GetValues(typeof (Priority))
                                     .Cast<Priority>()
                                     .ToDictionary(e => (int)e, e => Enum.GetName(typeof (Priority), e))
                                     .ToList();
        }
    }
}
