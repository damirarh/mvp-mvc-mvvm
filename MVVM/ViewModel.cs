﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using MVVM.Annotations;
using MVVM.Service;

namespace MVVM
{
    public class ViewModel : INotifyPropertyChanged
    {
        private ObservableCollection<TodoItem> _todoItems;
        private TodoItem _selectedItem;
        private bool _editMode;
        private TodoItem _displayItem;

        public ViewModel()
        {
            Init();

            NewCommand = new RelayCommand(OnNew);
            EditCommand = new RelayCommand(OnEdit);
            CancelCommand = new RelayCommand(OnCancel);
            SaveCommand = new RelayCommand(OnSave);
        }

        private void OnEdit()
        {
            DisplayItem = new TodoItem
                {
                    Id = DisplayItem.Id,
                    Title = DisplayItem.Title,
                    Description = DisplayItem.Description,
                    DueDate = DisplayItem.DueDate,
                    Priority = DisplayItem.Priority,
                    Done = DisplayItem.Done,
                };
            EditMode = true;
        }

        private async void OnSave()
        {
            using (var proxy = new TodoServiceClient())
            {
                var item = await proxy.SaveTodoItemAsync(DisplayItem);
                if (SelectedItem.Id == item.Id)
                {
                    SelectedItem.Title = item.Title;
                    SelectedItem.Description = item.Description;
                    SelectedItem.DueDate = item.DueDate;
                    SelectedItem.Priority = item.Priority;
                    SelectedItem.Done = item.Done;
                }
                else
                {
                    TodoItems.Add(item);
                }
            }
            EditMode = false;
        }

        private void OnCancel()
        {
            DisplayItem = SelectedItem;
            EditMode = false;
        }

        private void OnNew()
        {
            DisplayItem = new TodoItem();
            EditMode = true;
        }

        public async void Init()
        {
            using (var proxy = new TodoServiceClient())
            {
                TodoItems = new ObservableCollection<TodoItem>(await proxy.GetTodoItemsAsync());
            }
        }

        public ObservableCollection<TodoItem> TodoItems
        {
            get { return _todoItems; }
            set
            {
                if (Equals(value, _todoItems)) return;
                _todoItems = value;
                OnPropertyChanged();
            }
        }

        public TodoItem SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                if (Equals(value, _selectedItem)) return;
                _selectedItem = value;
                OnPropertyChanged();
                DisplayItem = SelectedItem;
            }
        }

        public TodoItem DisplayItem
        {
            get { return _displayItem; }
            set
            {
                if (Equals(value, _displayItem)) return;
                _displayItem = value;
                OnPropertyChanged();
            }
        }

        public Dictionary<Priority, string> Priorities
        {
            get
            {
                return
                    Enum.GetValues(typeof (Priority))
                        .Cast<Priority>()
                        .ToDictionary(e => e, e => Enum.GetName(typeof (Priority), e));
            }
        }

        public bool EditMode
        {
            get { return _editMode; }
            set
            {
                if (value.Equals(_editMode)) return;
                _editMode = value;
                OnPropertyChanged();
                OnPropertyChanged("ViewMode");
            }
        }

        public bool ViewMode
        {
            get { return !EditMode; }
        }

        public RelayCommand NewCommand { get; set; }
        public RelayCommand EditCommand { get; set; }
        public RelayCommand CancelCommand { get; set; }
        public RelayCommand SaveCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}