#MVP, MVC and MVVM Session Sample Applications

This a set of sample applications accompanying my session on MVP, MVC and MVVM design patterns. The solution consists of 4 projects:

- **WebService** is a simple WCF web service project used by all applications
- **MVP** is a WinForms application implementing MVP design pattern
- **MVC** is a ASP.NET MVC application implementing MVC design pattern
- **MVVM** is a WPF application implementing MVVM design pattern