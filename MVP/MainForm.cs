﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using MVP.Service;

namespace MVP
{
    public partial class MainForm : Form, IView
    {
        private Presenter _presenter;
        private TodoItem _displayItem;

        public MainForm()
        {
            InitializeComponent();

            todoItemsDataGridView.AutoGenerateColumns = false;

            priorityComboBox.ValueMember = "Key";
            priorityComboBox.DisplayMember = "Value";

            _presenter = new Presenter(this);
        }

        private void BindToDoItem()
        {
            idTextBox.DataBindings.Clear();
            titleTextBox.DataBindings.Clear();
            descriptionTextBox.DataBindings.Clear();
            dueDateDateTimePicker.DataBindings.Clear();
            priorityComboBox.DataBindings.Clear();
            doneCheckBox.DataBindings.Clear();

            if (DisplayItem != null)
            {
                idTextBox.DataBindings.Add("Text", DisplayItem, "Id");
                titleTextBox.DataBindings.Add("Text", DisplayItem, "Title");
                descriptionTextBox.DataBindings.Add("Text", DisplayItem, "Description");
                dueDateDateTimePicker.DataBindings.Add(new NullableDateBinding("Value", DisplayItem, "DueDate"));
                priorityComboBox.DataBindings.Add("SelectedValue", DisplayItem, "Priority");
                doneCheckBox.DataBindings.Add("Checked", DisplayItem, "Done");
            }
        }

        public IList<KeyValuePair<Priority, string>> Priorities
        {
            set { priorityComboBox.DataSource = value; }
        }

        public IList<TodoItem> TodoItems
        {
            get { return todoItemsDataGridView.DataSource as IList<TodoItem>; }
            set
            {
                todoItemsDataGridView.DataSource = new BindingList<TodoItem>(value.ToList());
            }
        }

        public TodoItem SelectedItem
        {
            get { return todoItemsDataGridView.SelectedRows.Count == 0 ? null : todoItemsDataGridView.SelectedRows[0].DataBoundItem as TodoItem; }
        }

        public TodoItem DisplayItem
        {
            get { return _displayItem; }
            set
            {
                if (Equals(value, _displayItem)) return;
                _displayItem = value;
                BindToDoItem();
            }
        }

        public bool EditMode
        {
            get { return todoPanel.Enabled; }
            set 
            { 
                todoPanel.Enabled = value;
                todoItemsDataGridView.Enabled = !value;
                commandsPanel.Enabled = !value;
            }
        }

        public event EventHandler SelectedItemChanged;
        public event EventHandler NewClicked;
        public event EventHandler EditClicked;
        public event EventHandler SaveClicked;
        public event EventHandler CancelClicked;

        private void todoItemsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (SelectedItemChanged != null)
                SelectedItemChanged(sender, e);
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            if (NewClicked != null)
            {
                NewClicked(sender, e);
            }
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (EditClicked != null)
            {
                EditClicked(sender, e);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (SaveClicked != null)
            {
                SaveClicked(sender, e);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (CancelClicked != null)
            {
                CancelClicked(sender, e);
            }
        }
    }
}
