﻿using System;
using System.Windows.Forms;

namespace MVP
{
    public class NullableDateBinding : Binding
    {
        public NullableDateBinding(string propertyName, object dataSource, string dataMember) 
            : base(propertyName, dataSource, dataMember, true)
        {
            Format += OnFormat;
            Parse += OnParse;
        }

        private void OnParse(object sender, ConvertEventArgs convertEventArgs)
        {
            var binding = sender as Binding;

            if (binding != null)
            {
                var dateTimePicker = (binding.Control as DateTimePicker);

                if (dateTimePicker != null)
                {
                    if (dateTimePicker.Checked == false)
                    {
                        dateTimePicker.ShowCheckBox = true;
                        dateTimePicker.Checked = false;
                        convertEventArgs.Value = null;
                    }
                    else
                    {
                        var val = Convert.ToDateTime(convertEventArgs.Value);
                        convertEventArgs.Value = val.Date;
                    }
                }
            }
        }

        private void OnFormat(object sender, ConvertEventArgs convertEventArgs)
        {
            var binding = sender as Binding;

            if (binding != null)
            {
                var dateTimePicker = (binding.Control as DateTimePicker);

                if (dateTimePicker != null)
                {
                    if (convertEventArgs.Value == null)
                    {
                        dateTimePicker.ShowCheckBox = true;
                        dateTimePicker.Checked = false;
                        convertEventArgs.Value = dateTimePicker.Value;
                    }
                    else
                    {
                        dateTimePicker.ShowCheckBox = true;
                        dateTimePicker.Checked = true;
                    }
                }
            }
        }
    }
}