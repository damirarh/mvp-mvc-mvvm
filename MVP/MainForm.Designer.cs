﻿namespace MVP
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.todoItemsDataGridView = new System.Windows.Forms.DataGridView();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titleColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.descriptionColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dueDateColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priorityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doneColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.todoPanel = new System.Windows.Forms.Panel();
            this.idLabel = new System.Windows.Forms.Label();
            this.idTextBox = new System.Windows.Forms.TextBox();
            this.titleLabel = new System.Windows.Forms.Label();
            this.titleTextBox = new System.Windows.Forms.TextBox();
            this.descriptionLabel = new System.Windows.Forms.Label();
            this.descriptionTextBox = new System.Windows.Forms.TextBox();
            this.dueDateLabel = new System.Windows.Forms.Label();
            this.dueDateDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.priorityLabel = new System.Windows.Forms.Label();
            this.priorityComboBox = new System.Windows.Forms.ComboBox();
            this.doneLabel = new System.Windows.Forms.Label();
            this.doneCheckBox = new System.Windows.Forms.CheckBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.commandsPanel = new System.Windows.Forms.Panel();
            this.editButton = new System.Windows.Forms.Button();
            this.newButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.todoItemsDataGridView)).BeginInit();
            this.todoPanel.SuspendLayout();
            this.commandsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // todoItemsDataGridView
            // 
            this.todoItemsDataGridView.AllowUserToAddRows = false;
            this.todoItemsDataGridView.AllowUserToDeleteRows = false;
            this.todoItemsDataGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.todoItemsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.todoItemsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.todoItemsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idColumn,
            this.titleColumn,
            this.descriptionColumn,
            this.dueDateColumn,
            this.priorityColumn,
            this.doneColumn});
            this.todoItemsDataGridView.Location = new System.Drawing.Point(13, 13);
            this.todoItemsDataGridView.MultiSelect = false;
            this.todoItemsDataGridView.Name = "todoItemsDataGridView";
            this.todoItemsDataGridView.ReadOnly = true;
            this.todoItemsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.todoItemsDataGridView.Size = new System.Drawing.Size(280, 291);
            this.todoItemsDataGridView.TabIndex = 0;
            this.todoItemsDataGridView.SelectionChanged += new System.EventHandler(this.todoItemsDataGridView_SelectionChanged);
            // 
            // idColumn
            // 
            this.idColumn.DataPropertyName = "Id";
            this.idColumn.HeaderText = "Id";
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            this.idColumn.Width = 41;
            // 
            // titleColumn
            // 
            this.titleColumn.DataPropertyName = "Title";
            this.titleColumn.HeaderText = "Title";
            this.titleColumn.Name = "titleColumn";
            this.titleColumn.ReadOnly = true;
            this.titleColumn.Width = 52;
            // 
            // descriptionColumn
            // 
            this.descriptionColumn.DataPropertyName = "Description";
            this.descriptionColumn.HeaderText = "Description";
            this.descriptionColumn.Name = "descriptionColumn";
            this.descriptionColumn.ReadOnly = true;
            this.descriptionColumn.Width = 85;
            // 
            // dueDateColumn
            // 
            this.dueDateColumn.DataPropertyName = "DueDate";
            this.dueDateColumn.HeaderText = "Due Date";
            this.dueDateColumn.Name = "dueDateColumn";
            this.dueDateColumn.ReadOnly = true;
            this.dueDateColumn.Width = 78;
            // 
            // priorityColumn
            // 
            this.priorityColumn.DataPropertyName = "Priority";
            this.priorityColumn.HeaderText = "Priority";
            this.priorityColumn.Name = "priorityColumn";
            this.priorityColumn.ReadOnly = true;
            this.priorityColumn.Width = 63;
            // 
            // doneColumn
            // 
            this.doneColumn.DataPropertyName = "Done";
            this.doneColumn.HeaderText = "Done";
            this.doneColumn.Name = "doneColumn";
            this.doneColumn.ReadOnly = true;
            this.doneColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.doneColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.doneColumn.Width = 58;
            // 
            // todoPanel
            // 
            this.todoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.todoPanel.Controls.Add(this.cancelButton);
            this.todoPanel.Controls.Add(this.saveButton);
            this.todoPanel.Controls.Add(this.doneCheckBox);
            this.todoPanel.Controls.Add(this.doneLabel);
            this.todoPanel.Controls.Add(this.priorityComboBox);
            this.todoPanel.Controls.Add(this.priorityLabel);
            this.todoPanel.Controls.Add(this.dueDateDateTimePicker);
            this.todoPanel.Controls.Add(this.dueDateLabel);
            this.todoPanel.Controls.Add(this.descriptionTextBox);
            this.todoPanel.Controls.Add(this.descriptionLabel);
            this.todoPanel.Controls.Add(this.titleTextBox);
            this.todoPanel.Controls.Add(this.titleLabel);
            this.todoPanel.Controls.Add(this.idTextBox);
            this.todoPanel.Controls.Add(this.idLabel);
            this.todoPanel.Enabled = false;
            this.todoPanel.Location = new System.Drawing.Point(299, 44);
            this.todoPanel.Name = "todoPanel";
            this.todoPanel.Size = new System.Drawing.Size(192, 267);
            this.todoPanel.TabIndex = 1;
            // 
            // idLabel
            // 
            this.idLabel.AutoSize = true;
            this.idLabel.Location = new System.Drawing.Point(4, 4);
            this.idLabel.Name = "idLabel";
            this.idLabel.Size = new System.Drawing.Size(16, 13);
            this.idLabel.TabIndex = 0;
            this.idLabel.Text = "Id";
            // 
            // idTextBox
            // 
            this.idTextBox.Location = new System.Drawing.Point(7, 21);
            this.idTextBox.Name = "idTextBox";
            this.idTextBox.ReadOnly = true;
            this.idTextBox.Size = new System.Drawing.Size(182, 20);
            this.idTextBox.TabIndex = 1;
            // 
            // titleLabel
            // 
            this.titleLabel.AutoSize = true;
            this.titleLabel.Location = new System.Drawing.Point(4, 44);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(27, 13);
            this.titleLabel.TabIndex = 2;
            this.titleLabel.Text = "Title";
            // 
            // titleTextBox
            // 
            this.titleTextBox.Location = new System.Drawing.Point(7, 60);
            this.titleTextBox.Name = "titleTextBox";
            this.titleTextBox.Size = new System.Drawing.Size(182, 20);
            this.titleTextBox.TabIndex = 3;
            // 
            // descriptionLabel
            // 
            this.descriptionLabel.AutoSize = true;
            this.descriptionLabel.Location = new System.Drawing.Point(4, 83);
            this.descriptionLabel.Name = "descriptionLabel";
            this.descriptionLabel.Size = new System.Drawing.Size(60, 13);
            this.descriptionLabel.TabIndex = 4;
            this.descriptionLabel.Text = "Description";
            // 
            // descriptionTextBox
            // 
            this.descriptionTextBox.Location = new System.Drawing.Point(7, 100);
            this.descriptionTextBox.Name = "descriptionTextBox";
            this.descriptionTextBox.Size = new System.Drawing.Size(182, 20);
            this.descriptionTextBox.TabIndex = 5;
            // 
            // dueDateLabel
            // 
            this.dueDateLabel.AutoSize = true;
            this.dueDateLabel.Location = new System.Drawing.Point(4, 123);
            this.dueDateLabel.Name = "dueDateLabel";
            this.dueDateLabel.Size = new System.Drawing.Size(53, 13);
            this.dueDateLabel.TabIndex = 6;
            this.dueDateLabel.Text = "Due Date";
            // 
            // dueDateDateTimePicker
            // 
            this.dueDateDateTimePicker.Checked = false;
            this.dueDateDateTimePicker.CustomFormat = "";
            this.dueDateDateTimePicker.Location = new System.Drawing.Point(7, 139);
            this.dueDateDateTimePicker.Name = "dueDateDateTimePicker";
            this.dueDateDateTimePicker.ShowCheckBox = true;
            this.dueDateDateTimePicker.Size = new System.Drawing.Size(182, 20);
            this.dueDateDateTimePicker.TabIndex = 0;
            // 
            // priorityLabel
            // 
            this.priorityLabel.AutoSize = true;
            this.priorityLabel.Location = new System.Drawing.Point(4, 162);
            this.priorityLabel.Name = "priorityLabel";
            this.priorityLabel.Size = new System.Drawing.Size(38, 13);
            this.priorityLabel.TabIndex = 7;
            this.priorityLabel.Text = "Priority";
            // 
            // priorityComboBox
            // 
            this.priorityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.priorityComboBox.FormattingEnabled = true;
            this.priorityComboBox.Location = new System.Drawing.Point(7, 178);
            this.priorityComboBox.Name = "priorityComboBox";
            this.priorityComboBox.Size = new System.Drawing.Size(182, 21);
            this.priorityComboBox.TabIndex = 8;
            // 
            // doneLabel
            // 
            this.doneLabel.AutoSize = true;
            this.doneLabel.Location = new System.Drawing.Point(4, 202);
            this.doneLabel.Name = "doneLabel";
            this.doneLabel.Size = new System.Drawing.Size(33, 13);
            this.doneLabel.TabIndex = 9;
            this.doneLabel.Text = "Done";
            // 
            // doneCheckBox
            // 
            this.doneCheckBox.AutoSize = true;
            this.doneCheckBox.Location = new System.Drawing.Point(7, 218);
            this.doneCheckBox.Name = "doneCheckBox";
            this.doneCheckBox.Size = new System.Drawing.Size(15, 14);
            this.doneCheckBox.TabIndex = 10;
            this.doneCheckBox.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(105, 238);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(84, 24);
            this.cancelButton.TabIndex = 12;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(7, 238);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(84, 24);
            this.saveButton.TabIndex = 11;
            this.saveButton.Text = "Save";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // commandsPanel
            // 
            this.commandsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.commandsPanel.Controls.Add(this.editButton);
            this.commandsPanel.Controls.Add(this.newButton);
            this.commandsPanel.Location = new System.Drawing.Point(299, 6);
            this.commandsPanel.Name = "commandsPanel";
            this.commandsPanel.Size = new System.Drawing.Size(192, 39);
            this.commandsPanel.TabIndex = 2;
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(105, 7);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(84, 24);
            this.editButton.TabIndex = 5;
            this.editButton.Text = "Edit";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // newButton
            // 
            this.newButton.Location = new System.Drawing.Point(7, 7);
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(84, 24);
            this.newButton.TabIndex = 4;
            this.newButton.Text = "New";
            this.newButton.UseVisualStyleBackColor = true;
            this.newButton.Click += new System.EventHandler(this.newButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(503, 316);
            this.Controls.Add(this.commandsPanel);
            this.Controls.Add(this.todoPanel);
            this.Controls.Add(this.todoItemsDataGridView);
            this.Name = "MainForm";
            this.Text = "Todo List";
            ((System.ComponentModel.ISupportInitialize)(this.todoItemsDataGridView)).EndInit();
            this.todoPanel.ResumeLayout(false);
            this.todoPanel.PerformLayout();
            this.commandsPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView todoItemsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn titleColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn descriptionColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dueDateColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn priorityColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn doneColumn;
        private System.Windows.Forms.Panel todoPanel;
        private System.Windows.Forms.DateTimePicker dueDateDateTimePicker;
        private System.Windows.Forms.Label dueDateLabel;
        private System.Windows.Forms.TextBox descriptionTextBox;
        private System.Windows.Forms.Label descriptionLabel;
        private System.Windows.Forms.TextBox titleTextBox;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.TextBox idTextBox;
        private System.Windows.Forms.Label idLabel;
        private System.Windows.Forms.Label doneLabel;
        private System.Windows.Forms.ComboBox priorityComboBox;
        private System.Windows.Forms.Label priorityLabel;
        private System.Windows.Forms.CheckBox doneCheckBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Panel commandsPanel;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button newButton;

    }
}

