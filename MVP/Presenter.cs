﻿using System;
using System.Linq;
using MVP.Service;

namespace MVP
{
    public class Presenter
    {
        private readonly IView _view;

        public Presenter(IView view)
        {
            _view = view;

            _view.Load += OnLoad;
            _view.SelectedItemChanged += OnSelectedItemChanged;

            _view.NewClicked += OnNewClicked;
            _view.EditClicked += OnEditClicked;
            _view.SaveClicked += OnSaveClicked;
            _view.CancelClicked += OnCancelClicked;
        }

        private void OnCancelClicked(object sender, EventArgs eventArgs)
        {
            _view.DisplayItem = _view.SelectedItem;
            _view.EditMode = false;
        }

        private async void OnSaveClicked(object sender, EventArgs eventArgs)
        {
            using (var proxy = new TodoServiceClient())
            {
                var item = await proxy.SaveTodoItemAsync(_view.DisplayItem);
                if (_view.SelectedItem.Id == item.Id)
                {
                    _view.SelectedItem.Title = item.Title;
                    _view.SelectedItem.Description = item.Description;
                    _view.SelectedItem.DueDate = item.DueDate;
                    _view.SelectedItem.Priority = item.Priority;
                    _view.SelectedItem.Done = item.Done;
                }
                else
                {
                    _view.TodoItems.Add(item);
                }
            }
            _view.EditMode = false;
        }

        private void OnEditClicked(object sender, EventArgs eventArgs)
        {
            _view.DisplayItem = new TodoItem
            {
                Id = _view.DisplayItem.Id,
                Title = _view.DisplayItem.Title,
                Description = _view.DisplayItem.Description,
                DueDate = _view.DisplayItem.DueDate,
                Priority = _view.DisplayItem.Priority,
                Done = _view.DisplayItem.Done,
            };
            _view.EditMode = true;
        }

        private void OnNewClicked(object sender, EventArgs eventArgs)
        {
            _view.DisplayItem = new TodoItem();
            _view.EditMode = true;
        }

        private void OnSelectedItemChanged(object sender, EventArgs eventArgs)
        {
            _view.DisplayItem = _view.SelectedItem;
        }

        private async void OnLoad(object sender, EventArgs eventArgs)
        {
            _view.Priorities = Enum.GetValues(typeof(Priority))
                                   .Cast<Priority>()
                                   .ToDictionary(e => e, e => Enum.GetName(typeof(Priority), e))
                                   .ToList();

            using (var proxy = new TodoServiceClient())
            {
                _view.TodoItems = await proxy.GetTodoItemsAsync();
            }
        }
    }
}