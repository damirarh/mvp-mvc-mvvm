﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using MVP.Service;

namespace MVP
{
    public interface IView
    {
        IList<KeyValuePair<Priority, string>> Priorities { set; }
        IList<TodoItem> TodoItems { get; set; }
        TodoItem SelectedItem { get; }
        TodoItem DisplayItem { get; set; }

        bool EditMode { get; set; }

        event EventHandler Load;
        event EventHandler SelectedItemChanged;

        event EventHandler NewClicked;
        event EventHandler EditClicked;
        event EventHandler SaveClicked;
        event EventHandler CancelClicked;
    }
}